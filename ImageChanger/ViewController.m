//
//  ViewController.m
//  ImageChanger
//
//  Created by 古林 俊祐 on 2015/03/09.
//  Copyright (c) 2015年 ShunsukeFurubayashi. All rights reserved.
//

#import "ViewController.h"

#import "SFCUtil.h"
#import "GPUImage.h"

@interface ViewController () <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet GPUImageView *gpuImageView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *convertButton;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;
@property (strong, nonatomic) UIImage *originalImage;

@end

@implementation ViewController

#pragma mark - Init
- (void)initView {
    self.imageView.layer.borderColor = [[UIColor colorWithRed:0.639 green:0.694 blue:0.698 alpha:1] CGColor];
    self.imageView.layer.borderWidth = 1;
    self.imageView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2f];
    self.imageView.userInteractionEnabled = YES;
    self.gpuImageView.hidden = YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]];
    
    [self.textFields enumerateObjectsUsingBlock:^(UITextField *obj, NSUInteger idx, BOOL *stop) {
        obj.delegate = self;
    }];
    
    NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
    // キーボード表示時
    [notification addObserver:self selector:@selector(keyboardWillShow:) name: UIKeyboardWillShowNotification object:nil];
    // キーボード非表示時
    [notification addObserver:self selector:@selector(keyboardWillHide:) name: UIKeyboardWillHideNotification object:nil];
    
    self.convertButton.layer.cornerRadius = 75.0f;
    self.convertButton.layer.masksToBounds = YES;
    self.convertButton.backgroundColor = [UIColor colorWithRed:0.275 green:0.690 blue:0.522 alpha:1];
}

#pragma mark - View Life Cycle
- (void)loadView {
    [super loadView];
    [self initView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - IBActions
- (IBAction)takePicture:(id)sender {
    UIActionSheet *ac = [UIActionSheet new];
    ac.delegate = self;
    ac.tag = 0;
    [ac addButtonWithTitle:@"カメラで撮影"];
    [ac addButtonWithTitle:@"ライブラリから選択"];
    [ac addButtonWithTitle:@"キャンセル"];
    ac.cancelButtonIndex = 2;
    [ac showInView:self.view];
}

- (IBAction)clearImage:(id)sender {
    UIActionSheet *ac = [UIActionSheet new];
    ac.delegate = self;
    ac.tag = 1;
    [ac addButtonWithTitle:@"リセット"];
    [ac addButtonWithTitle:@"キャンセル"];
    [ac addButtonWithTitle:@"キャンセル"];
    ac.cancelButtonIndex = 2;
    [ac showInView:self.view];
}

- (IBAction)clearFilter:(id)sender {
    [self.textFields enumerateObjectsUsingBlock:^(UITextField *obj, NSUInteger idx, BOOL *stop) {
        obj.text = @"";
    }];
}

- (IBAction)convertImage:(id)sender {
    if (self.imageView.image == nil) return;
    
    NSMutableArray *filter1 = [@[] mutableCopy];
    NSMutableArray *filter2 = [@[] mutableCopy];
    NSMutableArray *filter3 = [@[] mutableCopy];
    [self.textFields enumerateObjectsUsingBlock:^(UITextField *obj, NSUInteger idx, BOOL *stop) {
        NSString *filNumber;
        if (obj.text.length == 0) {
            filNumber = @"0";
        } else {
            filNumber = obj.text;
        }
        if (obj.tag < 3) {
            [filter1 addObject:filNumber];
        } else if (obj.tag < 6) {
            [filter2 addObject:filNumber];
        } else {
            [filter3 addObject:filNumber];
        }
    }];
    NSArray *filter = @[filter1, filter2, filter3];
    
    __weak ViewController *weakSelf = self;
    [self convertImage:self.imageView.image filter:filter onComplete:^(UIImage *image) {
        weakSelf.imageView.image = image;
    }];
}

- (IBAction)share:(id)sender {
    if (self.imageView.image == nil) return;
    
    UIImageWriteToSavedPhotosAlbum(self.imageView.image, self, nil, nil);
    [[[UIAlertView alloc] initWithTitle:@"" message:@"写真を保存しました。" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - UIActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 2) return;
    
    if (actionSheet.tag == 0) {
        
        UIImagePickerController *ipc = [UIImagePickerController new];
        ipc.delegate = self;
        ipc.allowsEditing = YES;
        
        if (buttonIndex == 0) {
            if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
            }
        } else if (buttonIndex == 1) {
            ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self presentViewController:ipc animated:YES completion:nil];
        }];
        
    } else {
        
        if (self.imageView.image == nil) return;
        
        if (buttonIndex == 0) {
            __weak ViewController *weakSelf = self;
            [SFCUtil convertGrayScaleImage:self.originalImage onComplete:^(UIImage *grayImage) {
                weakSelf.imageView.image = grayImage;
            }];
        } else {
            self.imageView.image = nil;
            self.cameraImageView.hidden = NO;
            self.closeButton.hidden = YES;
            self.originalImage = nil;
        }
    }
}

#pragma mark - UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    self.cameraImageView.hidden = YES;
    self.closeButton.hidden = NO;
    
    __weak ViewController *weakSelf = self;
    [SFCUtil resizeImageFromSize:info[UIImagePickerControllerEditedImage] withSize:CGSizeMake(650, 500) onComplete:^(UIImage *image) {
        weakSelf.originalImage = image;
        [SFCUtil convertGrayScaleImage:image onComplete:^(UIImage *grayImage) {
            weakSelf.imageView.image = grayImage;
        }];
    }];
}

#pragma mark - UITextField Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *str = textField.text;
    str = [str stringByReplacingCharactersInRange:range withString:string];
    
    NSCharacterSet *stringCharacterSet = [NSCharacterSet characterSetWithCharactersInString:str];
    NSCharacterSet *digitCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789.-"];
    if (![digitCharacterSet isSupersetOfSet:stringCharacterSet]) {
        return NO;
    }
    if ([str componentsSeparatedByString:@"."].count > 2) {
        return NO;
    }
    return YES;
}

#pragma mark - NSNotification
- (void)keyboardWillShow:(NSNotification *)notificatioin {
    NSDictionary *userInfo = [notificatioin userInfo];
    
    CGRect keyboardFrame;
    keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.view.frame = CGRectMake(0, -CGRectGetHeight(keyboardFrame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }];
}

- (void)keyboardWillHide:(NSNotification *)notificatioin{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.view.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame));
                     }];
}

#pragma mark - Image Manage
- (void)convertImage:(UIImage *)image filter:(NSArray *)filter onComplete:(void (^)(UIImage *image))complete {
    float nw = [filter[0][0] floatValue]; float n  = [filter[0][1] floatValue]; float ne = [filter[0][2] floatValue];
    float w  = [filter[1][0] floatValue]; float c  = [filter[1][1] floatValue]; float e  = [filter[1][2] floatValue];
    float sw = [filter[2][0] floatValue]; float s  = [filter[2][1] floatValue]; float se = [filter[2][2] floatValue];

    // 画像を作る
    GPUImagePicture *imagePicture = [[GPUImagePicture alloc] initWithImage:image];
    
    GPUImage3x3ConvolutionFilter *convolutionfilter = [[GPUImage3x3ConvolutionFilter alloc] init];
    [convolutionfilter setConvolutionKernel:(GPUMatrix3x3){
        { nw,  n, ne},
        { w,  c, e},
        { sw,  s, se}
    }];
    [imagePicture addTarget:convolutionfilter];
    [convolutionfilter addTarget:self.gpuImageView];
    [convolutionfilter useNextFrameForImageCapture];
    [imagePicture processImage];
    
    UIImage *filterImage = [convolutionfilter imageFromCurrentFramebuffer];
    self.imageView.image = filterImage;
}

@end
