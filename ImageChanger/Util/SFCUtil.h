//
//  SFCUtil.h
//  ImageChanger
//
//  Created by 古林 俊祐 on 2015/03/09.
//  Copyright (c) 2015年 ShunsukeFurubayashi. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

@interface SFCUtil : NSObject

+ (void)resizeImageFromSize:(UIImage *)image withSize:(CGSize)imageSize onComplete:(void (^)(UIImage *image))handler;
+ (void)convertGrayScaleImage:(UIImage*)image onComplete:(void (^)(UIImage *image))handler;

@end
