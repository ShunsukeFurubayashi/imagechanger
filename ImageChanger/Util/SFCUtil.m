//
//  SFCUtil.m
//  ImageChanger
//
//  Created by 古林 俊祐 on 2015/03/09.
//  Copyright (c) 2015年 ShunsukeFurubayashi. All rights reserved.
//

#import "SFCUtil.h"

@implementation SFCUtil

+ (void)resizeImageFromSize:(UIImage *)image withSize:(CGSize)imageSize onComplete:(void (^)(UIImage *image))handler {
    dispatch_queue_t gcd_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(gcd_queue, ^{
        
        float resized_x = 0.0;
        float resized_y = 0.0;
        float resized_width = imageSize.width;
        float resized_height = imageSize.height;
        
        float ratio_width = imageSize.width/image.size.width;
        float ratio_height = imageSize.height/image.size.height;
        if( ratio_width < ratio_height ){
            resized_width = image.size.width*ratio_height;
            resized_x = (imageSize.width-resized_width)/2;
        }else{
            resized_height = image.size.height*ratio_width;
            resized_y = (imageSize.height-resized_height)/2;
        }
        
        CGSize resized_size = CGSizeMake(imageSize.width ,imageSize.height);
        UIGraphicsBeginImageContext(resized_size);
        [image drawInRect:CGRectMake(resized_x, resized_y, resized_width, resized_height)];
        UIImage* resized_image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            handler(resized_image);
            
        });
    });
}

+ (void)convertGrayScaleImage:(UIImage *)image onComplete:(void (^)(UIImage *))handler {
    dispatch_queue_t gcd_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    dispatch_async(gcd_queue, ^{
        
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
        CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
        CGRect rect = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
        CGColorSpaceRelease(colorSpace);
        CGContextDrawImage(context, rect, [image CGImage]);
        CGImageRef grayscale = CGBitmapContextCreateImage(context);
        CGContextRelease(context);
        UIImage* grasyScaleImage = [UIImage imageWithCGImage:grayscale];
        CFRelease(grayscale);
         
        dispatch_async(dispatch_get_main_queue(), ^{
            
            handler(grasyScaleImage);
            
        });
    });
}

@end
